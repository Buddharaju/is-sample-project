@tag 
Feature: View outstanding IDV requests - IS browse screen 

As a Customer Administration Officer
I want to view outstanding IDV requests
So that I can select a request to process

Summary
Add Identity Verification Form under Customer screen. The Customer Administration Officer will use IDV Form to process IDV requests. 

Acceptance criteria:
1.User can view all Outstanding IDV requests

2.User can view information about the IDV request including:
	-Date/Time the request was created
	-The status of the request
	-The number of IDV attempts (out of 3 allowed attempts)
	-The Account number for the requester
	-The Requester's First name
	-The Requester's Last name
	-The Recommendation
	-The default sort is by date/time created

Scenario: IDV browser accessible from Customer screen 
	Given I am on Customer screen 
	And I view Identity Verification Forms 
	When I click IDV Browser 
	Then I am on IDV Browser screen 
	
Scenario: IDV browser screen on default
	Then the processing status is "Not processed" 
	And the count of outstanding IDV requests is "15"
	
Scenario: Identifying outstanding IDV requests that are Over Grace Period 
	Given I have multiple outstanding IDV requests that are not processed 
	And Over Grace Period is set to 30 days 
	And first IDV request created date is greater than 30 days grace period 
	And second IDV request created date is exactly 30 days grace period 
	And third IDV request created date is less than 30 days grace period 
	When I view outstanding requests 
	Then I can view the Over Grace Period for first IDV request as 'Yes' 
	And I can view the Over Grace Period for second IDV request as 'No' 
	And I can view the Over Grace Period for third IDV request as 'No' 
	
	#Scenario: Sort outstanding IDV requests that are Over Grace Period and date created 
	#	Given I have multiple outstanding IDV requests that are not processed 
	#		|Over Grace Period|Created Date|Processing Status|Account|First Name|Surname|Recommendation|Attempts|
	#		|Yes|25/10/2017 15:49:10|Not Processed|12345|Robert|Smith|FAIL|1|
	#		|Yes|25/10/2017 15:49:10|Not Processed|89900|Marge|Simpson|PASS|1|
	#		|No|25/10/2017 15:49:10|Not Processed|89999|Jim|O'Neil|FAIL|1|
	#	When I view outstanding requests 
	#	Then I can see the processing status as 'Not processed' 
	#	And I can view all outstanding IDV requests that are not processed 
	
	#Scenario Outline:Search IDV Requests by Processing Status, Created Date from, Created Date To 
	#	Given I have multiple outstanding IDV requests that are not processed: 
	#		|Over Grace Period|Created Date|Processing Status|Account|First Name|Surname|Recommendation|Attempts ?|
	#		|Yes|25/10/2017 15:49:10|Not Processed|12345|Robert|Smith|FAIL|1|
	#		|Yes|25/10/2017 15:49:10|Not Processed|89900|Marge|Simpson|PASS|1|
	#		|No|25/10/2017 15:49:10|Not Processed|89999|Jim|O'Neil|FAIL|1|
	#	When I filter IDV requests by "<Processing Status>" "<Account Number>" "<First Name>" "<Surname>" 
	#	Then I can view only "<No of oustanding>" idv request 
	#	Examples: 
	#		|Processing Status |Account Number|First Name|Surname|No of outstanding|
	#		|Not Processed|89989|Jean|Smith|1|
	#		|Not Processed|34512|Fred|Wilson|1|
	#	