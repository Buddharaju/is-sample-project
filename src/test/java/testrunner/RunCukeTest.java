package testrunner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import fixture.BaseFixture;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/stories", glue = "cucumber.steps", snippets = SnippetType.CAMELCASE,
        plugin = {"pretty", "html:target/Cucumber" , "json:target/cucumber.json", "junit:target/cucumber.xml"} )
public class RunCukeTest extends BaseFixture{
	
}
