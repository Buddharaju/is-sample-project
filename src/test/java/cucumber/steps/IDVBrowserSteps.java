package cucumber.steps;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Arrays;
import java.util.List;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fixture.BaseFixture;
import web.pages.Customer;
import web.pages.IDVBrowser;
import web.pages.InformationSystem;

public class IDVBrowserSteps extends BaseFixture {

	private static final String URL = "http://devsprtdsb043:8080/perl/global_home.pl?home_page=1";
	private Customer customer;
	private List<String> identityVerificationFormslist;
	private IDVBrowser idvbrowser;

	@Given("^I am on Customer screen$")
	public void iAmOnCustomerScreen() {
		driver.get(URL);
		InformationSystem informationSystem = new InformationSystem(driver);
		customer = informationSystem.clickCustomer();
	}

	@And("^I view Identity Verification Forms$")
	public void iViewIdentityVerificationForms() {
		identityVerificationFormslist = customer.getIDVformsList();
		List<String> expectedIDVlist = Arrays.asList("IDV Browser", "IDV Search", "IDV Maintenance Forms");
		assertThat("identity verification form list is not as expected", identityVerificationFormslist,
				is(expectedIDVlist));
	}

	@When("^I click IDV Browser$")
	public void iClickIDVBrowser() {
		idvbrowser = customer.clickIDVBrowser();
	}

	@Then("^I am on IDV Browser screen$")
	public void iAmOnIDVBrowserScreen() {
		assertThat("search results count is not 15", idvbrowser.getSearchResultsCount(), is(15));
	}

	@Then("^the processing status is \"([^\"]*)\"$")
	public void theProcessingStatusIs(String status) {
		assertThat("processing status is not " + status, idvbrowser.getProcessingStatus(), is(status));
	}

	@Then("^the count of outstanding IDV requests is \"([^\"]*)\"$")
	public void theCountOfOutstandingIDVRequestsIs(int countOfRequests) {
		assertThat("search results count is not " + countOfRequests, idvbrowser.getSearchResultsCount(),
				is(countOfRequests));
	}

}