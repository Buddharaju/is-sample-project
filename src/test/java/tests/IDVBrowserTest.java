package tests;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import fixture.BaseFixture;
import web.pages.Customer;
import web.pages.IDVBrowser;
import web.pages.InformationSystem;

public class IDVBrowserTest extends BaseFixture {
	private static final String URL = "http://devsprtdsb043:8080/perl/global_home.pl?home_page=1";

	@Test
	public void test_IDVformslist_on_customer_screen() {
		List<String> expectedIDVlist = Arrays.asList("IDV Browser", "IDV Search", "IDV Maintenance Forms");

		// given
		driver.get(URL);
		InformationSystem informationSystem = new InformationSystem(driver);
		Customer customer = informationSystem.clickCustomer();

		// when
		List<String> identityVerificationFormslist = customer.getIDVformsList();

		// then
		assertThat("identity verification form list is not as expected", identityVerificationFormslist,
				is(expectedIDVlist));

	}

	@Test
	public void test_default_idvbrowser_screen() throws InterruptedException {

		Customer customer = new Customer(driver);

		// when
		IDVBrowser idvbrowser = customer.clickIDVBrowser();

		// then
		assertThat("search results count is 15", idvbrowser.getSearchResultsCount(), is(15));
	}

}
