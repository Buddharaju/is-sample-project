package web.pages;

import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class InformationSystem extends PageObject {

	@FindBy(xpath = "//*[@class='env_dev']/table[1]//a[@class='subsystem']")
	private List<WebElement> is_menu;

	public InformationSystem(WebDriver driver) {
		super(driver);
	}

	public Customer clickCustomer() {
		is_menu.get(1).click();
		return new Customer(driver);
	}

	@Override
	protected ExpectedCondition<?> pageIsLoaded(Object... params) {
		return ExpectedConditions.visibilityOf(is_menu.stream().findAny().get());
	}

}
