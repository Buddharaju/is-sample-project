package web.pages;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Customer extends PageObject {

	@FindBy(xpath = "//*[@class='env_dev']/table[2]/tbody/tr/td[2]/table/tbody/tr[1]/td/table/tbody")
	private WebElement identity_verification_forms;

	public Customer(WebDriver driver) {
		super(driver);
	}

	public List<String> getIDVformsList() {
		ArrayList<String> idvformslist = new ArrayList<String>();

		List<WebElement> rowsList = identity_verification_forms.findElements(By.xpath("tr"));
		List<WebElement> columnsList = null;

		for (WebElement row : rowsList) {
			columnsList = row.findElements(By.xpath("td[2]/a"));
			for (WebElement column : columnsList) {
				idvformslist.add(column.getText());
			}
		}
	
		return idvformslist;
	}

	public IDVBrowser clickIDVBrowser() {
		clickIdentityVerificationFormsItem("IDV Browser");
		return new IDVBrowser(driver);
	}

	private void clickIdentityVerificationFormsItem(String ivf_item) {
		List<WebElement> rowsList = identity_verification_forms.findElements(By.xpath("tr"));
		List<WebElement> columnsList = null;
		outerloop: for (WebElement row : rowsList) {
			columnsList = row.findElements(By.xpath("td[2]/a"));
			for (WebElement column : columnsList) {
				if (column.getText().equals(ivf_item)) {
					column.click();
					break outerloop;
				}
			}
		}
	}

	@Override
	protected ExpectedCondition<?> pageIsLoaded(Object... params) {
		return ExpectedConditions.visibilityOf(identity_verification_forms);
	}

}
