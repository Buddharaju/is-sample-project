package web.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class IDVBrowser extends PageObject {

	@FindBy(xpath = "//*[@name='main_form']/table/tbody/tr[2]/td")
	private WebElement status;

	@FindBy(name = "fields_frame")
	private WebElement fields_frame;

	@FindBy(id = "search_results")
	private WebElement searchresults;

	@FindBy(className = "start_page_title_heading")
	private WebElement pageTitle;

	public IDVBrowser(WebDriver driver) {
		super(driver);
	}

	public int getSearchResultsCount() {
		driver.switchTo().frame(1);
		return searchresults.findElements(By.tagName("tr")).size() - 1;
	}

	public String getProcessingStatus() {
		Select dropdown = new Select(driver.findElement(By.id("status")));
		String selectedStatus = dropdown.getAllSelectedOptions().get(0).getText();
		return selectedStatus;
	}

	@Override
	protected ExpectedCondition<?> pageIsLoaded(Object... params) {
		return ExpectedConditions.visibilityOf(fields_frame);
	}

}
